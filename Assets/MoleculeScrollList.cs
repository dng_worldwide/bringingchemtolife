﻿/*
 * Name: MoleculeScrollList.cs
 * Description: MoleculeScrollList controls the creation of the scrolling list of molecule buttons. It houses the functionality of the molecule generation buttons. 
 * Please note that some of the variables in this class are set as public. These variables are set to some object in the unity environment through the Unity Enditor.
 * Programmer: Joseph Nguyen
 * Date: 3/30/2018
 * Team: DNGWorldWide
 * Project: Bringing Chemistry To Life
 * 
 * This file is part of Bringing Chemistry To Life.
 *
 *   Bringing Chemistry To Life is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Bringing Chemistry To Life is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Bringing Chemistry To Life.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

// includes various libraries from unity and c#
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;

// Class MoleculeScrollList
public class MoleculeScrollList : MonoBehaviour {

    public MoleculeManager manager; // stores a reference to the molecule manager gameobject
    private List<string> listOfMolecules = new List<string>(); // stores a list of all molecules that is read in from a json file. 
    private string objType = ".obj"; // file extension for a OBJ file.
    private Vector3 position; // stores a tuple pertaining to a molecule's position. (x , y , z) 
    public SimpleObjectPool buttonObjectPool; // reference to a object pool
    public Transform contentPanel; // stores a reference to the panel that the buttons will be on
    public GameObject prefab; // stores a reference to the button prefab created in unity.
    private bool firstObject; // checks to see if it is the first object generated in the scene.

    // Description: Start is a called when the scene is loaded in. It calls the loadComponents function to create the buttons and load in the molecule list for faster
    // access.
    // Pre: Scene is loaded in
    // Post: Waits for 1 second and loads the molecule list from the manager and creates buttons equal to number of molecules read in from the manager. 
    void Start()
    {
        Invoke("loadComponents", 1); 
    }
    // Description: Sets the molecule manager gameobject, loads the list of molecules, and creates and adds buttons to the canvas accordingly.
    // Pre: none
    // Post: All of the above from the description is executed without error. 
    public void loadComponents()
    {
        manager =  FindObjectOfType<MoleculeManager>();
        loadMoleculeList();
        addButtonsToCanvas();
        firstObject = false;
    }
    // Description: A list of molecules names is loaded from the molecule manager.
    // Pre: the molecule list from the manager is not null.
    // Post: The list of molecules names is populated.
    private void loadMoleculeList()
    {
        int moleculeListSize = manager.getMoleculeListSize(); // gets the size of the molecule list from manager and stores it.

        // loops and adds the name of the molecule to the list of molecule names.
        for (int i = 0; i < moleculeListSize; i++)
        {
            string name = manager.getMoleculeName(i); // gets the molecule name at some index i from the manager. 
            listOfMolecules.Add(name); // adds the molecule to the list. 
        }

    }
    // Description: Creates and adds buttons to the contentPanel canvas.
    // Pre: content panel is not null and is set in the unity editor.
    // Post: Buttons are created and added in the canvas based on how many items are in the list of molecules. 
    private void addButtonsToCanvas()
    {
        // Loops from 0 to the size of the list. 
        for(int i = 0; i < listOfMolecules.Count; i++)
        {
            GameObject newButton = (GameObject)Instantiate(prefab); // creates a game object from the prefab
            newButton.transform.SetParent(contentPanel,false); // sets the parent panel as contentPanel
            GenerationButton generationButton = newButton.GetComponent<GenerationButton>(); // sets the button up with scripts from the GenerationButton class.
            generationButton.SetUp(listOfMolecules[i], this); // finishes the set up of the button with the name of the molecule a molecule scroll list object. 
        }
    }

    // Description: Generates a new position for the molecule
    // Pre: none
    // Post: A new vector3 tuple is returned after being generated. 
    private void getNewPosition()
    {
        position = new Vector3(Random.Range(-4f, 0f), Random.Range(1.5f, 2f), Random.Range(0f, 1f)); // gets a random location.. 
    }

    // Description: Generates and spawns a molecule gameobject in the virtual environment.
    // Pre: String name is valid
    // Post: a molecule is spawned according to the button that gets pressed. 
    private void generateMolecule(string name)
    {
        string streamingAssets = Application.streamingAssetsPath; // Gets the streaming assets path.
        string filePath = Path.Combine(streamingAssets, name.ToLower()); // combines the path and the name of the molecule.
        filePath = filePath + objType; // adds the .obj to the end of the file name.
        // If the file exists 
        if (File.Exists(filePath))
        {            
            GameObject mol = OBJLoader.LoadOBJFile(filePath); // creates a game object and spawns it in the virtual environment;
            mol.transform.localScale -= new Vector3(0.9f, 0.9f, 0.9f); // scales the game object to a managable size

            // adding the appropriate components and scripts to the molecule object. 
            mol.AddComponent<SphereCollider>();
            mol.AddComponent<Rigidbody>();
            mol.AddComponent<VRTK.GrabAttachMechanics.VRTK_FixedJointGrabAttach>();
            mol.AddComponent<VRTK.VRTK_InteractableObject>();
            mol.AddComponent<VRTK.SecondaryControllerGrabActions.VRTK_SwapControllerGrabAction>();
          
            // Loops until there is no collision.
            do
            {

                // If there is a collision, get a different position.
                if (manager.checkCollision(mol) == true)
                {
                     getNewPosition(); // gets a new position
                }
                // if it is the first object, find a position for the molecule. 
                else if (firstObject == false)
                {
                    getNewPosition(); // gets a new position
                    firstObject = true; // sets the firstObject boolean to true. 
                }

                mol.transform.position = position;

            } while (manager.checkCollision(mol));

            // Adjusting settings for the each of the components when needed.

            // sets the grab mechanic script to be the object's fixed joint grab attach script.
            mol.GetComponent<VRTK.VRTK_InteractableObject>().grabAttachMechanicScript = mol.GetComponent<VRTK.GrabAttachMechanics.VRTK_FixedJointGrabAttach>();
            // sets isGrabbable to true.
            mol.GetComponent<VRTK.VRTK_InteractableObject>().isGrabbable = true;
            // sets isTrigger on the sphere collider to true.
            mol.GetComponent<SphereCollider>().isTrigger = true;
            //adjust radius of sphere collider to 1.0
            mol.GetComponent<SphereCollider>().radius = 1f;
            // turn off use gravity to allow molecule object to float.
            mol.GetComponent<Rigidbody>().useGravity = false;
            // turn on is kinematic.
            mol.GetComponent<Rigidbody>().isKinematic = true;
            

            string desc = manager.getMoleculeDescription(name); // gets the molecule description based on name.
            manager.addMolecule(mol, name, desc); // adds the molecule to the active molecule list.
        }
    }
    // Description: Button clicked function 
    // Pre: name is valid
    // Post: generateMolecule is called after the button gets clicked. 
    public void ButtonClicked(string name)
    {
        generateMolecule(name);
    }
   
}
