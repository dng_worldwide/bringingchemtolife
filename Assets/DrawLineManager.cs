﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * 
 * File: DrawLineManager.cs
 * Purpose: To allow users to draw lines within the game scene using the HTC Vive controllers.
 * Programmer: Joseph Nguyen
 * Team: DNG WorldWide
 * 
 * 
 * Note: All of this code is copied EXACTLY from a tutorial by FusedVR on Youtube. DNGWorldWide DOES NOT own or have written any of this code.
 * Please see the links below for the Youtube Video and to the tutorial on his website.
 * 
 * Youtube: https://www.youtube.com/watch?v=eMJATZI0A7c
 * 
 * Website: http://fusedvr.com/building-tilt-brush-from-scratch/
 * 
 * 
 * 
 * 
 * This file is part of Bringing Chemistry To Life.
 *
 *   Bringing Chemistry To Life is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Bringing Chemistry To Life is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Bringing Chemistry To Life.  If not, see <http://www.gnu.org/licenses/>.
 */
public class DrawLineManager : MonoBehaviour {

    public SteamVR_TrackedObject trackedObj; // stores an instance of the Vive controller.

    private MeshLineRenderer currLine; // stores an instance of the current line being drawn.

    private List<GameObject> lineList = new List<GameObject>(); // keeps track of all lines in the scene that have been drawn.

    private int numClicks = 0; // keeps track of how many points each line has. 
	
    // Description: Updates every frame. Checks for the trigger to be clicked on the controller.
    // Pre: A controller exists
    // Post: A line is drawn if the trigger is held. 
	void Update ()
    {
        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)trackedObj.index); // sets the device to the current tracked vive controller.
        // If the trigger pressed
        if (device.GetTouchDown (SteamVR_Controller.ButtonMask.Trigger) )
        {
            GameObject go = new GameObject(); // creates a new gameobject instance
            go.AddComponent<MeshFilter>(); // adds a meshfilter component to the gameobject
            go.AddComponent<MeshRenderer>(); // adds a meshrenderer component to the game object 
            currLine =  go.AddComponent<MeshLineRenderer>(); // Sets the current line to the gameobject's meshlinerenderer

            currLine.setWidth(.1f); // sets the width of the line to be .1 

            numClicks = 0; // sets the click to 0
            lineList.Add(go); // adds the line to the list
        }
        // if the trigger is held down
        else if(device.GetTouch(SteamVR_Controller.ButtonMask.Trigger))
        {
           
            currLine.AddPoint(trackedObj.transform.position); // sets the points 
            numClicks++; // adds the number of clicks
            
        }
        
	}
    // Description: The lines in the scene are deleted / destroyed. 
    // Pre: none
    // Post: Lines are erased/deleted/destroyed. 
    public void destroyLines()
    {
        foreach(GameObject line in lineList)
        {
            Destroy(line);
        }
    }

    
}
