﻿using UnityEngine;
using System.Collections;
/*
 * File: Answer.cs
 * Programmer: Joseph Nguyen
 * Description: Stores the data from the Json file.
 * Team: DNG WorldWide
 * 

 * 
 * Additional Notes: This class is heavily based on the AnswerData class from the Unity Quiz Game tutorial. Some code has been modified from the tutorial, but the
 * majority of the code comes from there. It can be found at the following link.
 * 
 * URL: https://unity3d.com/learn/tutorials/topics/scripting/intro-and-setup
 * 
 * 
 *   This file is part of Bringing Chemistry To Life.
 *
 *   Bringing Chemistry To Life is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Bringing Chemistry To Life is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Bringing Chemistry To Life.  If not, see <http://www.gnu.org/licenses/>.
 */
[System.Serializable]
public class Answer
{
    public string answerText;
    public bool isCorrect;

}