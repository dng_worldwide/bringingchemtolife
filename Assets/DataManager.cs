﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
/*
 * File: DataManager.cs
 * Programmer: Joseph Nguyen
 * Description: The DataManager class works to load the question data in from the json file.
 * Team: DNG WorldWide
 * 
 * This file is part of Bringing Chemistry To Life.
 *
 *   Bringing Chemistry To Life is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Bringing Chemistry To Life is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Bringing Chemistry To Life.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
public class DataManager : MonoBehaviour {

    public Question[] questionList;
    private string questionFilePath = "questions.json";
    
	// Description: The start function runs when the scene is loaded in.
    // Pre: none
    // Post: Game Data is loaded. 
	void Start ()
    {
        loadQuestionData();
	}
	

    // Description: Loads the question data in from a JSON file. 
    // Pre: none
    // Post: Question data is loaded in from the JSON file and is stored in a list. 
    private void loadQuestionData()
    {
        string filePath = Path.Combine(Application.streamingAssetsPath, questionFilePath); // File path to the location of the question json file.
        // Checks if the file exists at the given path
        if (File.Exists(filePath))
        {
            string jsonData = File.ReadAllText(filePath); // Reads all the text within the file

            QuestionList loadedQuestionData = JsonUtility.FromJson<QuestionList>(jsonData); // Creates a instance to store the question data read
            questionList = loadedQuestionData.questionData; // sets to the questionList     
            Debug.Log(questionList[0]);
        }
        else
        {
            Debug.LogError("Cannot load game data from file"); // Debug message
        }
    }
    
}
