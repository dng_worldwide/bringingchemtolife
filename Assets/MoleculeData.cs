﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * File: MoleculeData.cs
 * Description: Molecule data class to hold the name of the molecule and description. Uses System.Serializable to enable the JSON utility to store the data accordingly.
 * Programmer: Joseph Nguyen
 * Date: March 25th 2018
 * 
 * This file is part of Bringing Chemistry To Life.
 *
 *   Bringing Chemistry To Life is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Bringing Chemistry To Life is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Bringing Chemistry To Life.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
[System.Serializable]
public class MoleculeData
{
    public string name; // name of the molecule
    public string description; // stores the description about the molecules or other fun facts.
    // Description: Get function for the molecule name
    // Pre: none
    // Post: Name is returned. 
    public string getMoleculeName()
    {
        return name;
    }
    // Description: Get function for molecule description
    // Pre:none
    // Post: Description is returned. 
    public string getDescription()
    {
        return description;
    }
}
