﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using VRTK;
/*
 * Description: The MoleculeManager function is used to keep track of the number of molecules allowed in a scene. It will use an FIFO to determine which molecule
 * to remove once it hits the limit. It also keeps track of what information will be displayed on the TV screen in the VR Environment.
 * Programmer: Joseph Nguyen
 * Date: 3/13/18
 * Note: The public fields in this class have references to the gameobjects that can be set in the Unity Editor.
 * 
 * Disclaimer: The VRTK_InteractGrab script is property of TheStoneFox and one may find the source code for the script at the link below.
 * 
 * URL: https://github.com/thestonefox/VRTK/blob/master/Assets/VRTK/Scripts/Interactions/VRTK_InteractGrab.cs
 * 
 * This file is part of Bringing Chemistry To Life.
 *
 *   Bringing Chemistry To Life is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Bringing Chemistry To Life is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Bringing Chemistry To Life.  If not, see <http://www.gnu.org/licenses/>.
 */
public class MoleculeManager : MonoBehaviour
{
    private List<ActiveMolecules> activeMoleculeList = new List<ActiveMolecules>(); // Stores the molecules in the scene.
    private int molCount = 0; // Current count of the molecules in the scene.
    private int molLimit = 10; // Upper limit of the number of molecules allowed in the scene.
    private MoleculeData[] moleculeList; // stores the list of all the molecules that we have from the JSON file. 
    private string moleculeDataFileName = "molecules.json"; // stores the name of the molecule data file.
    public MoleculeDisplay display; // Reference to display gameobject in unity scene. 
    public MoleculeScrollList molScrollList; // references the molecule scroll list object.
    public VRTK.VRTK_InteractGrab right; // references the right controller object
    public VRTK.VRTK_InteractGrab left; // references the left controller object

    // Description: Loads molecule data from the JSON file.
    // Pre: scene is loaded in
    // Post: Molecule data is loaded in from the file and is stored in the moleculeList variable. 
    public void Start()
    {
        
        loadMoleculeData(); // loads the molecule data from the json file. 
        
    }
    // Description: Checks every frame for a gameobject being grabbed. The data that is displayed is the first object that is grabbed. 
    // Pre: none
    // Post: Some gameobject is grabbed and its data is displayed. 
    public void Update()
    {
        GameObject go; // stores a reference to a gameobject. 
        // If the right controller is empty and the left has an object
        if ((right.GetGrabbedObject() == null && left.GetGrabbedObject() != null) == true)
        {
            go = left.GetGrabbedObject(); // sets the gameobject to the molecule in the left hand. 
            displayData(go); // displays that game object's data. 
        }
        // If the right has an object and the left does not. 
        else if ((right.GetGrabbedObject() != null && left.GetGrabbedObject() == null))
        {
            go = right.GetGrabbedObject(); // sets the gameobject to the molecule in the right hand.
            displayData(go); // displays the gameobject data. 
        }
    }
    // Description: Gets the size of the moleculeList
    // Pre: none
    // Post: Returns the size.
    public int getMoleculeListSize()
    {
        return moleculeList.Length; 
    }
    // Description: Gets the molecule name at i
    // Pre: none 
    // Post: A molecule name is returned. 
    public string getMoleculeName(int i)
    {       
        return moleculeList[i].getMoleculeName();
    }
    // Description: Gets a description about the molecule.
    // Pre: name is valid
    // Post: a description is returned. 
    public string getMoleculeDescription(string name)
    {
        return findMoleculeDescriptionByName(name);   
    }
    // Description: Adds a molecule to the active molecule list. 
    // Pre: the parameters are valid. 
    // Post: Molecule is added to the list. 
    public bool addMolecule(GameObject mol, string name, string description)
    {
        ActiveMolecules newMol = new ActiveMolecules(mol, name,description); // creates a new active molecule object. 
        molCount = activeMoleculeList.Count; // gets the number of active molecules. 
        // if the count is lower than the limit.
        if (molCount < molLimit)
        {      
            activeMoleculeList.Add(newMol); // add the molecule to the active list.
            return true; // returns true. 
        }
        else
        {
            GameObject moleculeToDelete = activeMoleculeList[0].getMoleculeGameObject(); // gets the first molecule that was generated. 
            Destroy(moleculeToDelete); // destroy the gameobject.
            activeMoleculeList.RemoveAt(0); // Removes a molecule via FIFO.            
            activeMoleculeList.Add(newMol); // Adds the new molecule to the list of active molecules. 
            return false;
        }
    }
    // Description: Finds the molecule description by the name of the molecule 
    // Pre: none
    // Post: A molecule description is returned or null if it is not found
    public string findMoleculeDescriptionByName(string name)
    {
        // iterates the list of molecules
        for(int i = 0; i < moleculeList.Length; i++)
        {
            // if there is a match, return the description
            if (moleculeList[i].getMoleculeName() == name)
                return moleculeList[i].getDescription();
        }
        return null; // otherwise return nothing.
    }
    // Description: Searches for the molecule given a gameobject
    // Pre: The gameobject exists
    // Post: A molecule object is returned or null otherwise
    public ActiveMolecules findMoleculeByGameObject(GameObject go)
    {
        // iterates the list of active molecules
        for (int i = 0; i < activeMoleculeList.Count ; i++)
        {
            if (activeMoleculeList[i].getMoleculeGameObject() == go) // if there is a match, return the molecule. 
            {
                return activeMoleculeList[i];
            }
        }
        return null; // otherwise return null. 
    }
    // Description: Removes the molecule from the Scene
    // Pre: Gameobject exists in the scene
    // Post: Removes the molecule and reduces the count by 1
    public void removeMolecule(GameObject mol)
    {
        ActiveMolecules molToDelete = findMoleculeByGameObject(mol); // finds the molecule to be deleted 
        activeMoleculeList.Remove(molToDelete); // removes it from the active list
        molCount--; // decrement the count
    }
    // Description: Displays the moleculke 
    // Pre: Gameobject exists in the scene
    // Post: Information about the molecule is displayed in the scene.
    private void displayData(GameObject go)
    {
        ActiveMolecules curr = findMoleculeByGameObject(go);
        display.SetUp(curr.getMoleculeGameObject(), curr.getMoleculeName(), curr.getMoleculeDescription());

    }
    // Description: Loads the molecule data in from a JSON file. 
    // Pre: none
    // Post: Molecule data is loaded in from the JSON file and is stored in a list. 
    private void loadMoleculeData()
    {
        string filePath = Path.Combine(Application.streamingAssetsPath, moleculeDataFileName); // File path to the location of the molecule data file.

        // Checks if the file exists at the given path
        if (File.Exists(filePath))
        {
            string jsonData = File.ReadAllText(filePath); // Reads all the text within the file
            MoleculeList loadedMoleculeData = JsonUtility.FromJson<MoleculeList>(jsonData); // Creates a instance to store the molecule data read
            moleculeList = loadedMoleculeData.allMolecules; // sets to the moleculeList.           
        }
        else
        {
            Debug.LogError("Cannot load game data from file"); // Debug message
        }
    }
    // Description: Checks molecule collisions
    // Pre: Molecule exists in the scene
    // Post: If there is a collision, true is returned, otherwise false
    public bool checkCollision(GameObject newMol)
    {
        Collider newCollider, currMolCollider; // stores references to colliders. 

        // If the number of active molecules is greater than 0, 
        if (activeMoleculeList.Count > 0)
        {
            // for each molecule in the list
            foreach (ActiveMolecules go in activeMoleculeList)
            {
                currMolCollider = go.getMoleculeGameObject().GetComponent<Collider>(); // get the objects colider
                newCollider = newMol.GetComponent<Collider>(); // gets objects collider
                if (newCollider.bounds.Intersects(currMolCollider.bounds)) // checks the bounds intersections, if collision, return true
                        return true;
            }
        }

        return false; // no collision, return false
    }
}
