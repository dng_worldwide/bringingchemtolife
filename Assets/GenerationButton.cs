﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*
 * File: GenerationButton.cs
 * Description: Script for button functionality and set up. Functions include a button click and Setup functions. 
 * Programmer: Joseph Nguyen
 * Date: 3/29/2018
 * Team: DNG World Wide.
 * Note: All public fields have been set in the unity editor. 
 * 
 * This file is part of Bringing Chemistry To Life.
 *
 *   Bringing Chemistry To Life is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Bringing Chemistry To Life is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Bringing Chemistry To Life.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
public class GenerationButton : MonoBehaviour {

    public Button button; // stores a reference to a button prefab
    public Text moleculeName;  // Textfield for the molecule name.
    private MoleculeScrollList moleculeScrollList; // stores a reference to the molecule scroll list object
    // Description: Initialization function 
    // Pre: none
    // Post: Button gets functionality added when clicked. 
    void Start()
    {
        button.onClick.AddListener(buttonClicked); // Adds an action listener to the button for the function buttonClicked. 
    }
    // Description: Button setup function
    // Pre: Parameters are valid
    // Post: Sets the fields with data. 
    public void SetUp(string molName, MoleculeScrollList molList)
    {
        moleculeName.text = molName;
        moleculeScrollList = molList;
    }
    // Description: Calls the moleculeScroll list button clicked function
    // Pre: none
    // Post: The description is executed. 
    public void buttonClicked()
    {
        moleculeScrollList.ButtonClicked(moleculeName.text.ToString()); // calls the button click function in molecule scroll list. 
    }
	
}
