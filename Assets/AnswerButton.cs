﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*
 * File: AnswerButton.cs
 * Programmer: Joseph Nguyen
 * Description: AnswerButton class provides the answer buttons with functionality.
 * Team: DNG WorldWide
 * Date: 4-28-18
 * 
 * Notes: This class is heavily based on the AnswerButton class from the Unity Quiz Game tutorial. Some code has been modified from the tutorial, but the
 * majority of the code comes from there. It can be found at the following link.
 * 
 * URL: https://unity3d.com/learn/tutorials/topics/scripting/intro-and-setup
 * 
 * 
 * 
 * This file is part of Bringing Chemistry To Life.
 *
 *   Bringing Chemistry To Life is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Bringing Chemistry To Life is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Bringing Chemistry To Life.  If not, see <http://www.gnu.org/licenses/>.
 */
public class AnswerButton : MonoBehaviour {

    public Text answerText; // stores an instance of the button text.

    private Answer answerData; // stores the answer data
    private GuidedActivityManager manager;  // stores an instance of GuidedActivityManager.
    // Description: Setup function for all the fields in a button instance. 
    // Pre: none
    // Post: All fields are populated with data. 
	public void setup(Answer data, GuidedActivityManager newManager)
    {
        answerData = data; // sets answerData to data
        answerText.text = data.answerText; // updates the button text
        manager = newManager; // sets manager.
    }

    // Description: Calls the manager ButtonClicked function
    // Pre: none
    // Post: Answer correctness is checked. 
    public void ButtonClicked()       
    {
        manager.ButtonClicked(answerData.isCorrect);
    }
}
