﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * File: QuitGame.cs
 * Description: Allows users to quit the application.
 * Programmer: Joseph Nguyen
 * Date: 3/29/2018
 * Team: DNG WorldWide.
 * 
 *  This file is part of Bringing Chemistry To Life.
 *
 *   Bringing Chemistry To Life is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Bringing Chemistry To Life is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Bringing Chemistry To Life.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * 
 * 
 * 
 * 
 */
public class QuitGame : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
    public void QuitApplication()
    {
        Application.Quit();
    }
}
