﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using VRTK;
/*
 * File: SwitchScenes.cs
 * Description: Allows users to switch between all the different scenes in the software. 
 * Programmer: Joseph Nguyen
 * Date: 3/29/2018
 * Team: DNG WorldWide.
 * 
 *  This file is part of Bringing Chemistry To Life.
 *
 *   Bringing Chemistry To Life is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Bringing Chemistry To Life is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Bringing Chemistry To Life.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * 
 * 
 * 
 * 
 */
public class SwitchScenes : MonoBehaviour
{

    /*
     * Description: Allows users to navigate back to the Main Menu
     * Pre: User is in a scene that is not the main menu
     * Post: Scene is switched from the scene they are in to the main menu. 
     */
    public void switchToMainMenu()
    {
        setSDKManager();
        SceneManager.LoadScene("MainMenu"); // Scene Manager API loads the scene and switches the user over. 
    }
    /*
     * Description: Allows users to navigate to the Free Interaction mode screen.
     * Pre: User is in some scene
     * Post: Scene is switched from the scene they are in to the Free Interaction mode
     */
    public void switchToFreeInteraction()
    {
        setSDKManager();
        SceneManager.LoadScene("FreeInteraction");
    }
    /*
     * Description: Allows users to navigate back to the Guided Activity
     * Pre: User is in some scene that 
     * Post: Scene is switched from the scene they are in to the Guided Activity
     */
    public void switchToGuidedActivity()
    {
        setSDKManager();
        SceneManager.LoadScene("GuidedActivity");
    }
    /*
    * Description: Allows users to navigate back to the Student Portal
    * Pre: User is in some scene that 
    * Post: Scene is switched from the scene they are in to the Student Portal
    */
    public void switchToStudentPortal()
    {
        setSDKManager();
        SceneManager.LoadScene("StudentPortal");
    }
    /*
     * Description: Sets SDK Manager to false. This function fixes the issue of the UI buttons not working after loading to some scenes. Solution was found here: https://answers.unity.com/questions/1437486/vrtk-ui-doesnt-work-after-load-scene.html
     * Pre: None.
     * Post: SDK Manager Instance is set to false.
     * 
     */
    private void setSDKManager()
    {
        VRTK_SDKManager.instance.enabled = false;
    }
}