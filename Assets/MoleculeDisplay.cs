﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*
 * File: MoleculeDisplay.cs
 * Description: Script used to display data in the scene. Fields includes a molecule manager, gameobject, and text fields. 
 * Programmer: Joseph Nguyen
 * Date: 3/29/2018
 * Team: DNG World Wide.
 * Note: All public fields have been set in the unity editor. 
 * 
 * This file is part of Bringing Chemistry To Life.
 *
 *   Bringing Chemistry To Life is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Bringing Chemistry To Life is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Bringing Chemistry To Life.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
public class MoleculeDisplay : MonoBehaviour {

    public MoleculeManager manager; // stores a reference to a molecule manager object 
    private GameObject currentMolecule; // the current gameobject selected. 
    public Text moleculeNameText; // molecule name text
    public Text moleculeDescriptionText; // molecule description text 
	// Description: Acts as a setup object and text update function.
    // Pre: All parameters are valid
    // Post: Text in the scene is updated. 
	public void SetUp(GameObject go, string name, string description)
    {
        currentMolecule = go;
        moleculeNameText.text = name;
        moleculeDescriptionText.text = description;
    }
    // Description: Deletes the molecule from the scene
    // Pre: none
    // Post: Molecule is deleted
    public void deleteMolecule()
    {
        manager.removeMolecule(currentMolecule); // Removes molecule from the list
        Destroy(currentMolecule); // removes molecule from the scene
        clearText(); // clears the textfields related to the molecules. 
    }
    // Description: Clears the text 
    // Pre: None
    // Post: Text is cleared. 
    private void clearText()
    {
        moleculeNameText.text = "";
        moleculeDescriptionText.text = "";
    }
}
