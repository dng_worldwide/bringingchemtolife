﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * File: ActiveMolecules.cs
 * Description: Class to store data about the active molecules. Variables include the corresponding gameobject, name, and description. Functions include gets and a 
 * constructor. 
 * Programmer: Joseph Nguyen
 * Date: 3/29/2018
 * Team: DNG WorldWide.
 * 
 * This file is part of Bringing Chemistry To Life.
 *
 *   Bringing Chemistry To Life is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Bringing Chemistry To Life is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Bringing Chemistry To Life.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 */
public class ActiveMolecules
{
    private GameObject moleculeGameObject; // stores the molecule gameobject
    private string name; // stores the molecule name
    private string description; // stores the molecule description

    // Description: Gets the gameobject
    // Pre: none
    // Post: Gameobject is returned
    public GameObject getMoleculeGameObject()
    {
        return moleculeGameObject;
    }
    // Description: Gets the molecule name
    // Pre: none
    // Post: Molecule name is returned. 
    public string getMoleculeName()
    {
        return name;
    }
    // Description: Gets the molecule description
    // Pre: none
    // Post: Molecule description is returned. 
    public string getMoleculeDescription()
    {
        return description;
    }
    // Description: Constructor function for ActiveMolecule class
    // Pre: Gameobject exists, molecule name is valid, and description is valid
    // Post: All fields are set. 
    public ActiveMolecules(GameObject go, string newMoleculeName, string desc)
    {
        moleculeGameObject = go;
        name = newMoleculeName;
        description = desc;
    }
}
