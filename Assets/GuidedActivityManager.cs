﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
/*
 * File: GuidedActivityManager.cs
 * Programmer: Joseph Nguyen
 * Description: The Guided Activity Manager serves to interate through each of the guided activity questions, to give interaction and function to the buttons within 
 * the scene.
 * Team: DNG WorldWide
 * Date: April 28th 2018
 * Note: StartTimer is borrowed from a post on the Unity Forums found at the following link. It has been modified slightly for usage 
 * in this project. 
 * 
 * URL: https://answers.unity.com/questions/1174761/20-minute-countdown-timer.html
 * 
 * Additional Notes: This class is heavily based on the GameController class from the Unity Quiz Game tutorial. Some code has been modified from the tutorial, but the
 * majority of the code comes from there. It can be found at the following link.
 * 
 * URL: https://unity3d.com/learn/tutorials/topics/scripting/intro-and-setup
 * 
 * 
 * This file is part of Bringing Chemistry To Life.
 *
 *   Bringing Chemistry To Life is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Bringing Chemistry To Life is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Bringing Chemistry To Life.  If not, see <http://www.gnu.org/licenses/>.
 */
public class GuidedActivityManager : MonoBehaviour
{

    private float timer = 900;  // Set initial 15 minute timer. 
    public Text timerText; // stores the instance of timerText from the Unity scene.
    public Text questionText; // stores the instance of questionText from the Unity Scene
    public Transform answerButtonCanvas; // stores an instance of the answer button canvas
    public Transform deskCanvas; // stores an instance of the deskCanvas
    public DataManager dataManager; // stores an instance of DataManager
    private int questionIndex; // keeps track of the question index
    private Question[] questionList; // Stores the question data in an array.
    public Button startButton; // stores an instance of the start button
    private AnswerButton answerButton; // stores an instance of answerButton,
    public List<Button> answerButtonsList; // list of buttons from the scene.
    public Button restartButton; // stores an instance of restartButton.
    private bool hasReset = false; // keeps track if the user has used their additional 15 minutes yet. 
    private List<GameObject> spawnedMolecules =  new List<GameObject>(); // keeps track of the spawned molecules.
    private bool hasStarted = false; // checks if the guided activity has started yet.
    public DrawLineManager drawLineManager; // stores an instance of DrawLineManager.
    private  string molecule; // stores the molecule name.
    // Description: Start function. Waits 1 second to load to allow DataManager to load in the data first
    // Pre: none
    // Post: Data is loaded in.
    void Start ()
    {
        Invoke("loadData", 1); // Calls loadData after a 1 second delay.
        
	}

    // Description: Keeps track of the time and various bool variables. Disables buttons if the time runs out. Updates timer if there is still time.
    // Pre: none
    // Post: All things to keep track of in the description are tracked. 
    void Update ()
    {
        // if the timer is > 0 and the start button has been pressed.
        if (timer > 0f && hasStarted)
        {
            updateTimer(); // update the timer. 
        }
        // Else time has run out. 
        else if (timer < 0f)
        {
            questionText.text = "No time left, please exit this mode by \n selecting either Free Interaction or Student Portal."; // Displays the ending message.
            disableButtons(); // disables the answer buttons. 
        }
        // checks if the user has reset the timer yet. 
        if(hasReset)
        {
            restartButton.enabled = false; // if they have, then disable the restart button.
        }
    }
    // Description: Starts the quiz portion of the guided activity 
    // Pre: none
    // Post: Quiz gets started.
    public void startQuiz() 
    {
        hasStarted = true; // sets to true
        questionIndex = 0; // sets the questionIndex to 1.
        startTimer(); // starts the timer
        startButton.enabled = false; // disables the start button.
        restartButton.enabled = true; // enables the restartButton
        hasReset = false; // sets to false
        showQuestion(); // shows the next question.
        
    }
    // Description: Timer gets another 15 minutes added.
    // Pre: none
    // Post:  Timer is 15 minutes more than the current time.
    public void restartTimer() 
    {
        timer += 900f; // adds 900 seconds or 15 minutes
        hasReset = true; // sets to true
    }
    // Description: Updates the timer text.
    // Pre: none
    // Post: Updates and displays the new time on each call. 
    private void updateTimer()
    {
        if(timerText != null)
        {
            timer -= Time.deltaTime;
            string minutes = Mathf.Floor(timer / 60).ToString("00");
            string seconds = (timer % 60).ToString("00");
            timerText.text = "Time Left: " + minutes + ":" + seconds;
            
        }
    }
    // Description: Removes the molecule from the scene.
    // Pre: none
    // Post: Molecule is removed. 
    private void destroyMolecules()
    {
        foreach (GameObject go in spawnedMolecules)
        {
            Destroy(go);
        }
    }
    // Description: Starts the timer.
    // Pre: none
    // Post: Timer is set by default to 15 minutes and is started.
    private void startTimer()
    {
        if(timerText != null)
        {
            timer = 900;
            timerText.text = "Time: 15:00";
            
           
        }
    }
    // Description: Loads the question data from the data manager.
    // Pre: none
    // Post: Data is loaded and stored in questionList
    private void loadData() 
    {
        questionList = dataManager.questionList;
    }
    // Description: Displays the next question.
    // Pre: none
    // Post: The next question is displayed.
    private void showQuestion()
    {
        enableButtons(); // enables the answer buttons.
        Question questionData = questionList[questionIndex]; // sets the instance of question from questionList at questionIndex
        questionText.text = questionData.questionText; // updates the question text.
        
        // sets up the answer buttons
        for(int i = 0; i < answerButtonsList.Count; i++)
        {
            AnswerButton answerButton = answerButtonsList[i].GetComponent<AnswerButton>(); // adds the answerbutton component.
            answerButton.setup(questionData.answers[i],this); // sets up the fields.
        }
        spawnMolecules(); // spawns the relevant molecules
    }
    // Description: Lines drawn in the scene are deleted. 
    // Pre: none
    // Post:  Lines are deleted. 
    public void deleteLines()
    {
        drawLineManager.destroyLines();
    }
    // Description: Molecules relevant to the current question are spawned. 
    // Pre: none
    // Post:  Molecules are spawned. 
    private void spawnMolecules()
    {
        string assetPath = Application.streamingAssetsPath; // sets the asset path
        string obj = ".obj"; // obj file type. 
        string filePath; // stores the file path.
        molecule = questionList[questionIndex].molecule; // gets the molecule name
        
            filePath = Path.Combine(assetPath, molecule); // creates the file path.
            filePath = filePath + obj; // adds the file type to the file.
            Vector3 position; // Tuple to store x y and z coordinates
            // if file exists.
            if (File.Exists(filePath))
            { 
                GameObject mol = OBJLoader.LoadOBJFile(filePath); // creates a game object and spawns it in the virtual environment;
            mol.transform.localScale -= new Vector3(0.9f, 0.9f, 0.9f); // scales the game object to a managable size

            // adding the appropriate components and scripts to the molecule object. 
            mol.AddComponent<SphereCollider>();
                mol.AddComponent<Rigidbody>();
                mol.AddComponent<VRTK.GrabAttachMechanics.VRTK_FixedJointGrabAttach>();
                mol.AddComponent<VRTK.VRTK_InteractableObject>();
                mol.AddComponent<VRTK.SecondaryControllerGrabActions.VRTK_SwapControllerGrabAction>();

                position = new Vector3(-3.6f, 1.4f, 0f); // gets a random location..: Note: Adjust this for GA

                // Adjusting settings for the each of the components when needed.
                mol.transform.position = position;
                // sets the grab mechanic script to be the object's fixed joint grab attach script.
                mol.GetComponent<VRTK.VRTK_InteractableObject>().grabAttachMechanicScript = mol.GetComponent<VRTK.GrabAttachMechanics.VRTK_FixedJointGrabAttach>();
                // sets isGrabbable to true.
                mol.GetComponent<VRTK.VRTK_InteractableObject>().isGrabbable = true;
                // sets isTrigger on the sphere collider to true.
                mol.GetComponent<SphereCollider>().isTrigger = true;
                //adjust radius of sphere collider to 1.0
                mol.GetComponent<SphereCollider>().radius = 1f;
                // turn off use gravity to allow molecule object to float.
                mol.GetComponent<Rigidbody>().useGravity = false;
                // turn on is kinematic.
                mol.GetComponent<Rigidbody>().isKinematic = true;

                spawnedMolecules.Add(mol);
            }
        
    }
    // Description: Disables the answer buttons.
    // Pre: none
    // Post: Both buttons are disabled. 
    private void disableButtons()
    {
        answerButtonsList[0].enabled = false;
        answerButtonsList[1].enabled = false;
    }
    // Description: Enables the answer buttons
    // Pre: none
    // Post:  Both buttons are enabled. 
    private void enableButtons()
    {
        answerButtonsList[0].enabled = true;
        answerButtonsList[1].enabled = true;
    }
    // Description:
    // Pre:
    // Post: 
    public void ButtonClicked(bool isCorrect)
    {
        disableButtons();
        // if answer is correct. 
        if (isCorrect)
        {
            questionText.text = "Correct!";
            
        }
        // answer is incorrect
        else
        {
            questionText.text = "Incorrect!";
            

        }
        // destroys the molecules
        destroyMolecules();
        drawLineManager.destroyLines(); // removes the lines.
        // if not last question
        if (questionList.Length > questionIndex + 1)
        {
            questionIndex++; // increments the index
            Invoke("showQuestion", 2f); // shows the next question after a 2 second delay.
        }
        // else end game.
        else
        {
            Invoke("finishingMessage", 2f); // displays the ending message after a 2 second delay
        }
    }
    // Description: Displays the finishing message.
    // Pre: none
    // Post: Finishing message is displayed. 
    private void finishingMessage()
    {
        questionText.text = "Congratulations! You finished!\n Click start to try again. "; // message is updated. 
        disableButtons(); // disable answer buttons.
        startButton.enabled = true; // enables the start button
        restartButton.enabled = false; // disables the timer reset button. 
    }
}
